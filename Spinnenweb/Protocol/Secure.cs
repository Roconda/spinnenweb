﻿using System;
using System.Net.Sockets;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using Mono.Security;
using MSX = Mono.Security.X509;

using Mono.Security.Protocol.Tls;
using System.Security.Authentication;
using System.IO;
using System.Security.Cryptography;
using Mono.Security.Authenticode;

namespace Spinnenweb
{
	public sealed class Secure
	{
		private static volatile MSX.PKCS12 ssl;
		private static object locker = new object();

		private Secure() {}

		private static MSX.PKCS12 SSL
		{
			get 
			{
				if (ssl == null) 
				{
					lock (locker) 
					{
						if (ssl == null) 
							ssl = MSX.PKCS12.LoadFromFile ("Certificaat/pollux.p12", "s3kr3t");
					}
				}

				return ssl;
			}
		}

		public static Stream CreateStream(NetworkStream stream)
		{
			// alleen voor Nanne
			if (Environment.UserName != "nanne")
				return stream;

			SslServerStream sslStream = new SslServerStream(stream, new X509Certificate(SSL.Certificates[0].RawData));
			sslStream.PrivateKeyCertSelectionDelegate += (certificate, targetHost) => SSL.Keys [0] as RSA;
			return sslStream;
		}

	}
}

