using System;
using System.Net.Sockets;
using System.Text;
using System.Net;
using System.Threading;
using System.Linq;
using System.IO;
using Mono.Security.Protocol.Tls;

namespace Spinnenweb
{
	public class Listener
	{
		Socket socket;
		IRouter router;
		SemaphoreSlim sema;
		bool isSecure;

		public Listener (Socket handler, IRouter router, SemaphoreSlim sema, bool secure)
		{
			this.socket = handler;
			this.router = router;
			this.sema = sema;
			this.isSecure = secure;
		}

		public void Do() {
			lock (socket) {
				Handle ();

				socket.Close ();
				sema.Release ();
			}
		}

		public void Handle() {
			using(Stream connectionStream = isSecure ? Secure.CreateStream (new NetworkStream(socket)) : new NetworkStream(socket)) {
				var logEntry = new StringBuilder();

				if(connectionStream.CanRead){
					var reader = new StreamReader (connectionStream);

					try {
						if (reader.Peek () < 0)
							return;
					} catch(IOException e) {
						return;
					}

					long logStartTime = DateTime.Now.Ticks / 10000;						
					Response response;
					var request = new Request ();

					try {
						bool isFirst = true;
						while (reader.Peek () >= 0) {
							var line = reader.ReadLine();
							if (!String.IsNullOrEmpty(line))
							{
								if (isFirst) {
									request.ParseStatusLine (line);
									isFirst = false;
								} else {
									request.ParseHeader (line);
								}
							}
							else
							{
								break;
							}
						}
						request.ParseCookies ();
					} catch(ParseException e) {
						return;
					}

					var coleng = request.getHeader ("Content-Length") ?? "0";
					int length = Convert.ToInt32(coleng);
					var buffer = new char[length];

					reader.Read(buffer, 0, buffer.Length);
					request.Body = new String(buffer);

					request.ClientIP = socket.RemoteEndPoint.ToString();
					try {

						response = router.Router (request);

						logEntry.Append(
							response.status + " " +
							request.ClientIP + " " +
							request.getHeader("Host") + 
							request.RequestUri + " " + 
							DateTime.Now.ToString("yyyy-MM-dd-HH:mm:ss").ToString() + " "
						);
					} catch(Exception e) {
						response = Response.Error500 (e);
						logEntry.Append(
							response.status + " " +
							e.StackTrace + " " +
							e.Message + " " + 
							DateTime.Now.ToString("yyyy-MM-dd-HH:mm:ss").ToString() + " "
						);
					}

					var responseData = response.ToStream();

					byte[] b1 = System.Text.Encoding.UTF8.GetBytes (responseData.Item1);
					connectionStream.Write(b1, 0, b1.Length);
					try {
						responseData.Item2.CopyTo (connectionStream);
					} catch(IOException e){
						// ignore
					}
					responseData.Item2.Close ();

					// Calculate response speed and log to logger
					logEntry.Append(((DateTime.Now.Ticks / 10000) - logStartTime).ToString() + "ms");
					Logger.buffer.Add(logEntry.ToString());

					

				}else Console.WriteLine("Could not read connection");

			}
		}
	}
}