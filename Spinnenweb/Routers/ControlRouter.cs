﻿using System;
using System.Linq;
using System.IO;

namespace Spinnenweb
{
	public class ControlRouter : IRouter
	{
		Settings settings;

		public ControlRouter (Settings s)
		{
			settings = s;
		}


		#region IServer implementation

		public Response Router (Request request)
		{
			if (!(request.Method == Method.GET || request.Method == Method.POST))
				return Response.Error400 ();

			var db = new Database ();
			User? user = null;

			Boolean loggedIn = false;

			String sessKey = "";
			if (request.Cookies.ContainsKey ("session")) {
				sessKey = request.Cookies ["session"];
				user = db.IsLoggedIn (sessKey);
				loggedIn = user.HasValue;
			}

			if (!loggedIn) {
				if (request.RequestUri != "/") {
					return Response.Error404 (request.RequestUri);
				}
				return Login (request);
			}

			if (request.RequestUri == "/") {
				return ControlPage (request, user);
			}
			if (request.RequestUri == "/logs") {
				return LogPage (request);
			}
			if (request.RequestUri == "/logout") {
				return Logout (request, sessKey);
			}
			if (user.Value.Role == "beheerder") {
				if (request.RequestUri.StartsWith("/user")) {
					return UsersRouter.Router(request);
				}
			}
			return Response.Error404 (request.RequestUri);
		}

		#endregion
		private Response ControlPage (Request request, User? user) 
		{
			var response = new Response ();
			response.addHeader("Content-Type", "text/html; charset=utf-8");

			var page = new Control ();
			page.Model = settings;
			page.Secret = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(user.Value.Password));


			if(request.Method == Method.POST && user.Value.Role == "beheerder" && page.Secret == request.QueryString().GetValues("Secret").First()) {
				var post = request.QueryString();

				// Can't both run on the same port
				if(Convert.ToInt32(post.GetValues("WebPort").First()) != Convert.ToInt32(post.GetValues("ControlPort").First())) {
					settings.WebPort = Convert.ToInt32(post.GetValues("WebPort").First());
					settings.ControlPort = Convert.ToInt32(post.GetValues("ControlPort").First());
				}
				settings.WebRoot = post.GetValues("WebRoot").First();
				settings.DefaultPage = post.GetValues("DefaultPage").First();
				settings.DirectoryBrowsing = post.GetValues ("DirectoryBrowsing") != null && post.GetValues ("DirectoryBrowsing").FirstOrDefault() == "true";

				settings.saveSettings();
			}

			var output = page.GenerateString ();

			response.SetContent (output);
			return response;
		}

		static Response Login (Request request)
		{
			var response = new Response ();
			response.addHeader("Content-Type", "text/html; charset=utf-8");
			response.status = HTTPCodes.FORBIDDEN;
			String error = null;
			if(request.Method == Method.POST) {
				var db = new Database ();

				var post = request.QueryString ();
				var username = post.GetValues ("username").First ();
				var password = post.GetValues ("password").First ();

				User? s = db.Login (username, password);
				if (s.HasValue) {
					response.Cookies ["session"] = s.Value.SessionKey;
					response.Redirect ("/");
					return response;
				} else {
					error = "Verkeerde gebruikersnaam of wachtwoord";
				}
			}

			var login = new Login ();
			login.Error = error;
			var generated = login.GenerateString ();

			response.SetContent (generated);
			return response;
		}

		static Response Logout (Request request, String sesskey)
		{
			var db = new Database ();
			var response = new Response ();
			response.Cookies ["session"] = "";
			response.Redirect ("/");
			db.Logout (sesskey);

			return response;
		}

		static Response LogPage(Request request){
			var response = new Response ();
			var logPage = new LogReader();

			response.addHeader("Content-Type", "text/html; charset=utf-8");

			var file = File.Open (LogWriter.LOG_FILE, FileMode.OpenOrCreate, FileAccess.Read, FileShare.ReadWrite);
			string line;
			using (TextReader reader = new StreamReader(file)){
				while((line = reader.ReadLine()) != null){
					logPage.Model += line+"\n";
				}
			}

			file.Close();

			response.SetContent(logPage.GenerateString());
			return response;
		}
	}
}

