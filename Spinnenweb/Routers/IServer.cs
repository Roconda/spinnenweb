﻿using System;

namespace Spinnenweb
{
	public interface IRouter
	{
		Response Router (Request request);
	}
}

