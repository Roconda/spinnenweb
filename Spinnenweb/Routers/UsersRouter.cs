﻿using System;
using System.Data.Entity;

namespace Spinnenweb
{
	public static class UsersRouter
	{
		public static Response Router (Request request) {
			if (request.RequestUri == "/users" && request.Method == Method.GET) {
				return List(request);
			}

			if (request.RequestUri == "/users/edit" && request.Method == Method.GET) {
				return Edit(request);
			}

			if (request.RequestUri == "/users/edit" && request.Method == Method.POST) {
				return Update(request);
			}

			if (request.RequestUri == "/users/new" && request.Method == Method.GET) {
				return New(request);
			}

			if (request.RequestUri == "/users/new" && request.Method == Method.POST) {
				return Create(request);
			}

			if (request.RequestUri == "/users/delete" && request.Method == Method.GET) {
				return Delete(request);
			}


			return Response.Error404 (request.RequestUri);
		}

		 static Response List (Request request) {
			var db = new Database ();
			var response = new Response ();
			response.addHeader("Content-Type", "text/html; charset=utf-8");

			var list = new Users ();

			list.Model = db.ListUsers ();

			response.SetContent(list.GenerateString());

			return response;
		}

		static Response Delete (Request request) {
			var response = new Response ();

			var id = Int32.Parse (request.Query.Get("id"));
			var db = new Database ();
			db.DeleteUser (id);

			response.Redirect("/users");
			return response;
		}

		static Response Edit (Request request) {
			var response = new Response ();
			response.addHeader("Content-Type", "text/html; charset=utf-8");

			var id = Int32.Parse (request.Query.Get("id"));

			var db = new Database ();


			var userForm = new UserForm ();
			userForm.Model = db.GetUser (id);

			response.SetContent(userForm.GenerateString());

			return response;
		}

		static Response Update (Request request) {
			var response = new Response ();
			var id = Int32.Parse (request.Query.Get("id"));

			var db = new Database ();
			var post = request.QueryString ();

			db.ChangeUser (id, post.Get ("username"), post.Get ("password"), post.Get ("role"));

			response.Redirect("/users");
			return response;
		}

		public static Response New (Request request) {
			var response = new Response ();
			response.addHeader("Content-Type", "text/html; charset=utf-8");

			var db = new Database ();
			var userForm = new UserForm ();
			userForm.Model = new User();

			response.SetContent(userForm.GenerateString());

			return response;
		}

		 static Response Create (Request request) {
			var response = new Response ();
			response.Redirect("/users");
			var db = new Database ();
			var post = request.QueryString ();

			var username = post.Get ("username");
			var password = post.Get ("password");
			var role = post.Get ("role");

			db.InsertUser (username, password, role);

			return response;
		}
	}
}

