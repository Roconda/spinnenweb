﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace Spinnenweb
{
	public class WebRouter : IRouter
	{
		Settings setting;

		public WebRouter (Settings setting)
		{
			this.setting = setting;
		}

		#region IServer implementation

		public Response Router (Request request)
		{
			if (request.Method != Method.GET)
				return Response.Error400 ();

			string path = Path.Combine(setting.WebRoot, request.RequestUri.Substring(1));

			// No traversing
			var userPath = Path.GetFullPath((new Uri(path)).LocalPath);
			if(userPath.Length < setting.WebRoot.Length) {
				request.RequestUri = setting.WebRoot;
				path = setting.WebRoot;
			}

			if (!File.Exists (path) && !Directory.Exists(path)) {
				return Response.Error404(path);
			}
			FileAttributes attr = File.GetAttributes(path);

			//detect whether its a directory or file
			if ((attr & FileAttributes.Directory) == FileAttributes.Directory) {
				var pages = setting.DefaultPage.Split(new []{';'}).ToList();
				if (pages.Count == 0) {
					pages.Add (setting.DefaultPage);
				}
				foreach (var page in pages) {
					string pagePath = Path.Combine(path, page);
					if(File.Exists(pagePath)) {
						return Open (pagePath);
					}
				}

				if (setting.DirectoryBrowsing) {
					return ListDir (path, request);
				} else {
					return Response.Error403 (path);
				}
			} else {
				return Open (path);
			}
		}

		Response Open(String path) {
			var response = new Response ();
			String ext = Path.GetExtension (path).Substring(1);

			if (ContentType.Types.ContainsKey (ext)) {
				var type = ContentType.Types[ext];
				response.addHeader("Content-Type", type);
			} else {
				response.addHeader("Content-Type", "application/octet-stream");
				//response.addHeader("Content-Disposition", "attachment; filename=\"picture.png\"");
			}


			response.SetContent (File.Open(path, FileMode.Open, FileAccess.Read, FileShare.Read));
			return response;
		}

		#endregion

		Response ListDir (string path, Request request)
		{
			var list = Directory.EnumerateFileSystemEntries (path);

			var page = new FileDirectory ();
			page.Model = list.Select(x => {
				var y = x.Remove(0,path.Length);
				return y;
			}).ToList();

			page.Path = request.RequestUri;

			var output = page.GenerateString();

			var response = new Response ();
			response.addHeader("Content-Type", "text/html; charset=utf-8");
			response.SetContent (output);
			return response;
		}
	}
}

