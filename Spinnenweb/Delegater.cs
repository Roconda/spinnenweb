using System;
using System.Net.Sockets;
using System.Text;
using System.Net;
using System.Threading;

namespace Spinnenweb
{
	public class Delegater
	{
		Thread myThread;
		Func<IRouter> server;
		public int port { get; private set; }
		private Socket socket;
		private SemaphoreSlim stopSema;
		private bool isStopping = false;
		private object lockbject = new object();

		bool isSecure = false;

		public void Open(int port, Func<IRouter> server, bool isSecure) {
			this.port = port;
			this.server = server;
			this.isSecure = isSecure;
		}

		public void Run() {
			var myThreadDelegate = new ThreadStart(this.Server);
			myThread = new Thread(myThreadDelegate);
			myThread.Start();
		}

		public void Server() {
			socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			//var hostIP = Dns.GetHostEntry (IPAddress.Any).AddressList[0];
			var hostIP = IPAddress.Any;
			var ep = new IPEndPoint(hostIP, port);

			socket.Bind(ep); 
			socket.Listen (Constants.CONNECTIONS);

			var sema = new SemaphoreSlim (Constants.THREADS, Constants.THREADS);

			while (!isStopping) {
				sema.Wait ();
				try{
					var handler = socket.Accept();
					lock (lockbject) {
						var listener = new Listener (handler, server (), sema, isSecure);
						var subThread = new Thread (new ThreadStart (listener.Do));
						subThread.Start ();
					}
				}catch(SocketException e) {
					if (e.SocketErrorCode != SocketError.Interrupted && isStopping) {
						throw e;
					}
				}
			}
			stopSema.Release ();
		}

		public void Stop(SemaphoreSlim stopSema) {
			this.stopSema = stopSema;

			lock (lockbject) {

				isStopping = true;
				socket.Close ();
			}
		}
	}
}
