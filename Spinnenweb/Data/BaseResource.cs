using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO;

namespace Spinnenweb
{
	public class BaseResource
	{
		protected List<Tuple<string, string>> headers = new List<Tuple<string, string>>();
		protected Stream content = new MemoryStream(Encoding.UTF8.GetBytes("")) ;

		public BaseResource ()
		{
		}


		public virtual List<string> getHeaders(string name) {
			return headers.Where (t => t.Item1 == name).Select(x => x.Item2).ToList();
		}

		public virtual void addHeader(string type, string content){
			headers.Add(new Tuple<string,string>(type, content));
		}

		public virtual string getHeader(string type) {
			return getHeaders(type).FirstOrDefault();
		}

		public virtual void SetContent(string content) {
			this.content = new MemoryStream(Encoding.UTF8.GetBytes(content ?? ""));
		}

		public virtual void SetContent(FileStream content) {
			this.content = content;//this.content.Append(content);
		}



//		public new string ToString() {
//			StringBuilder finalContent = new StringBuilder();
//
//			finalContent.Append("HTTP/1.0 "+ status +"\r\n");
//			this.addHeader("Content-Length", content.Length.ToString());
//			foreach(Tuple<string, string> entry in headers) {
//				finalContent.Append(entry.Item1 + ": " + entry.Item2 + "\r\n");
//			}
//
//			finalContent.Append("\r\n"); // seperate headers from body
//
//			new StringReader (finalContent);
//
//			finalContent.Append(content.ToString());
//			return finalContent.ToString();
//		}
	}
}

