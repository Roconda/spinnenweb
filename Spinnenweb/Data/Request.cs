﻿using System;
using System.Text;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.Collections.Specialized;

namespace Spinnenweb
{
	public sealed class Request : BaseResource
	{
		public string Body {
			get;
			set;
		}

		public Method Method { get; private set; }
		public String RequestUri { get; set; }
		public String ClientIP { get; set; }
		public String HttpVersion { get; private set; }

		public NameValueCollection Query {
			get;
			private set;
		}

		public Dictionary<String, String> Cookies = new Dictionary<String, String> ();


		public Request () :base()
		{
		}


		public Request (String inboundLine) : base()
		{
			using (var reader = new StringReader(inboundLine))
			{
				String requestLine = reader.ReadLine ();
				ParseStatusLine (requestLine);

				String header = reader.ReadLine ();
				while (header != "") {
					ParseHeader (header);

					header = reader.ReadLine ();
				}

				ParseCookies ();
			}
		}


		public void ParseStatusLine(String statusLine) {

			String[] request = statusLine.Split (' ');
			if (request.Length != 3)
				throw new ParseException ();

			// Method
			switch (request [0]) {
			case "OPTIONS": Method = Method.OPTIONS; break;
			case "GET": Method = Method.GET; break;
			case "HEAD": Method = Method.HEAD; break;
			case "POST": Method = Method.POST; break;
			case "PUT": Method = Method.PUT; break;
			case "DELETE": Method = Method.DELETE; break;
			default:
				Method = Method.UNKNOWN;
				break;
			}

			var url = request [1].Split (new char[]{ '?' }, 2);
			if (url.Length != 2) {
				RequestUri = request [1];
			} else {
				RequestUri = url [0];
				Query = HttpUtility.ParseQueryString (url[1]);
			}

			HttpVersion = request [2];
		}


		public void ParseHeader (String header)
		{
			String[] hd = header.Split (new [] {':', ' '}, 2);
			if (hd.Length != 2)
				throw new ParseException ();

			addHeader (hd [0], hd [1]);
		}

		public void ParseCookies ()
		{
			var cookies = getHeader ("Cookie") ?? "";

			String[] split = cookies.Split (new []{';', ' '}, StringSplitOptions.None);
			if (split.Length == 0) {
				split = new []{ cookies };
			}

			foreach (var cookie in split) {
				String[] values = cookie.Split (new []{'='}, 2);
				if (values.Length == 2)
					Cookies [Uri.UnescapeDataString(values [0])] = Uri.UnescapeDataString(values [1]);
			}

		}

		public NameValueCollection QueryString() 
		{

			var res = HttpUtility.ParseQueryString (Body);
			return res;

		}

	}
}
