using System;

namespace Spinnenweb
{
	public class HTTPCodes
	{
		public static string MOVE_TEMPORARILY = "302 Moved Temporarily";
		public static string OK = "200 OK";
		public static string BAD_REQUEST = "400 Bad Request";
		public static string NOT_FOUND = "404 Not Found";
		public static string SERVER_ERROR = "500 Server Error";
		public static string FORBIDDEN = "403 Forbidden";
	}
}

