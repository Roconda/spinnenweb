﻿using System;

namespace Spinnenweb
{
	public enum Method
	{
		OPTIONS,
		GET,
		HEAD,
		POST,
		PUT,
		DELETE,

		UNKNOWN
	}
}

