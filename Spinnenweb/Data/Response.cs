﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO;

namespace Spinnenweb
{
	public class Response : BaseResource
	{
		public string status = HTTPCodes.OK;

		public Dictionary<String, String> Cookies = new Dictionary<String, String> ();

		public Response () :base()
		{
			// zie https://www.owasp.org/index.php/List_of_useful_HTTP_headers
			addHeader("X-Content-Type-Options", "nosniff");
			addHeader("X-Frame-Options", "deny");
			addHeader("X-XSS-Protection", "1; mode=block");
			addHeader("Content-Security-Policy", "default-src 'none'; frame-src 'none'; object-src 'none'; connect-src 'none'; img-src 'none'; style-src 'none' 'unsafe-inline'");
		}

		public void Redirect (string str)
		{
			this.status = HTTPCodes.MOVE_TEMPORARILY;
			addHeader ("Location", str);
		}

		public static Response Error400 ()
		{
			var response = new Response();
			response.addHeader("Content-Type", "text/html; charset=utf-8");
			response.status = HTTPCodes.BAD_REQUEST;
			var notfound = new Forbidden();
			var generated = notfound.GenerateString ();
			response.SetContent (generated);
			return response;
		}

		public static Response Error403 (string path)
		{
			var response = new Response();
			response.addHeader("Content-Type", "text/html; charset=utf-8");
			response.status = HTTPCodes.FORBIDDEN;
			var notfound = new Forbidden();
			notfound.Model = path;
			var generated = notfound.GenerateString ();
			response.SetContent (generated);
			return response;
		}

		public static Response Error404(String path) {
			var response = new Response();
			response.addHeader("Content-Type", "text/html; charset=utf-8");
			response.status = HTTPCodes.NOT_FOUND;
			var notfound = new NotFound();
			notfound.Model = path;
			var generated = notfound.GenerateString ();
			response.SetContent (generated);
			return response;
		}

		public static Response Error500(Exception excep) {
			var response = new Response();
			response.addHeader("Content-Type", "text/html; charset=utf-8");
			response.status = HTTPCodes.SERVER_ERROR;
			var error = new Error();
			//error.Model = excep;
			var generated = error.GenerateString ();
			response.SetContent (generated);
			return response;
		}

		public Tuple<string, Stream> ToStream() {
			StringBuilder finalContent = new StringBuilder();

			finalContent.Append("HTTP/1.0 "+ status +"\r\n");
			foreach (var item in Cookies) {
				if(item.Key != null && item.Value != null) {
					addHeader ("Set-Cookie", Uri.EscapeDataString(item.Key) + "="+ Uri.EscapeDataString(item.Value));
				}
			}
			this.addHeader("Content-Length", content.Length.ToString());
			foreach(Tuple<string, string> entry in headers) {
				finalContent.Append(entry.Item1 + ": " + entry.Item2 + "\r\n");
			}

			finalContent.Append("\r\n"); // seperate headers from body

			return new Tuple<string,Stream>(finalContent.ToString(), content);
		}
	}
}

