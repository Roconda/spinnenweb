﻿using System;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Security;

namespace Spinnenweb
{
	public class Settings
	{
		private string CONFIG_FILE = "Config/configuration.xml";
		private string ELEMENT_WEB_PORT = "web-port";
		private string ELEMENT_CONTROL_PORT = "control-port";
		private string ELEMENT_WEB_ROOT = "web-root";
		private string ELEMENT_DEFAULT_PAGE = "default-page";
		private string ELEMENT_DIRECTORY_BROWSING = "directory-browsing";

		public Settings ()
		{
			loadSettings();
		}

		private void loadSettings() {
			try{
				using (XmlReader reader = XmlReader.Create(CONFIG_FILE)){
					reader.MoveToContent();

					while(reader.Read()){
						if(reader.NodeType == XmlNodeType.Element) {
							if(reader.Name == ELEMENT_WEB_PORT) {
								reader.Read();
								WebPort = Convert.ToInt32(reader.Value);
							}else if(reader.Name == ELEMENT_CONTROL_PORT) {
								reader.Read();
								ControlPort = Convert.ToInt32(reader.Value);
							}else if(reader.Name == ELEMENT_WEB_ROOT) {
								reader.Read();
								WebRoot = reader.Value;
							}else if(reader.Name == ELEMENT_DEFAULT_PAGE) {
								reader.Read();
								DefaultPage = reader.Value;
							}else if(reader.Name == ELEMENT_DIRECTORY_BROWSING) {
								reader.Read();
								DirectoryBrowsing = Convert.ToBoolean(reader.Value);
							}
						}
					}

					reader.Close();
				}
			}finally{

			}
		}

		private int webport;
		public int WebPort {
			get {
				return webport;
			} 
			set{
				if(value > 1 && value <= 65535) webport = value;
			}
		 }
		private int controlport;
		public int ControlPort { 
			get {
				return controlport;
			} 
			set{
				if(value > 1 && value <= 65535) controlport = value;
			}
		}
		private string webroot;
		public string WebRoot { 
			get{
				return webroot;
			}
			set{
				webroot = SecurityElement.Escape(value);
			}
		 }
		private string defaultpage;
		public string DefaultPage { 
			get{
				return defaultpage;
			}
			set{
				defaultpage = SecurityElement.Escape(value);
			}
		}
		private bool directorybrowsing;
		public bool DirectoryBrowsing { 
			get{
				return directorybrowsing;
			}
			set{
				directorybrowsing = value;
			}
		}

		public void saveSettings() {
			try{
				var document = XElement.Load(CONFIG_FILE);
				document.XPathSelectElement("//"+ELEMENT_WEB_PORT).Value = Convert.ToString(WebPort);
				document.XPathSelectElement("//"+ELEMENT_CONTROL_PORT).Value = Convert.ToString(ControlPort);
				document.XPathSelectElement("//"+ELEMENT_WEB_ROOT).Value = Convert.ToString(WebRoot);
				document.XPathSelectElement("//"+ELEMENT_DEFAULT_PAGE).Value = Convert.ToString(DefaultPage);
				document.XPathSelectElement("//"+ELEMENT_DIRECTORY_BROWSING).Value = Convert.ToString(DirectoryBrowsing);
				document.Save(CONFIG_FILE);

				MainClass.RestartServers();
			}catch(Exception e){
				Console.WriteLine(e.Message + " " + e.StackTrace);
			}
		}

	}
}

