﻿using System;
using System.Threading;
using System.Diagnostics;

namespace Spinnenweb
{
	class MainClass
	{
		public static Settings Settings = new Settings();
		public static Thread LogThread;
		public static Thread LogWriteThread;
		public static Delegater WebDel;
		public static Delegater ControlDel;

		public static void Main (string[] args)
		{
			Console.WriteLine ("Initiating spinnenweb");


			var l = new Logger();
			var lw = new LogWriter();

			LogThread = new Thread (l.Run);
			LogThread.Start();

			LogWriteThread = new Thread (lw.Run);
			LogWriteThread.Start();

			StartServers();
		}

		public static void StartServers() {
			Debug.Assert (Settings.WebPort != Settings.ControlPort);

			StartWeb();
			StartControl();
		}

		public static void StartWeb() {
			Debug.Assert (Settings.WebPort != Settings.ControlPort);

			WebDel = new Delegater ();
			WebDel.Open (Settings.WebPort, () => new WebRouter(Settings), false);
			WebDel.Run ();
		}

		public static void StartControl() {
			Debug.Assert (Settings.WebPort != Settings.ControlPort);

			ControlDel = new Delegater ();
			ControlDel.Open (Settings.ControlPort, () => new ControlRouter(Settings), true);
			ControlDel.Run ();
		}

		public static void RestartServers() {
			var sema = new SemaphoreSlim (0, 2);

			if(Settings.WebPort != WebDel.port && Settings.ControlPort != ControlDel.port){
				WebDel.Stop(sema);
				ControlDel.Stop(sema);

				sema.Wait ();
				sema.Wait ();

				StartServers();
			}else if(Settings.WebPort != WebDel.port) {
				sema.Release ();
				WebDel.Stop(sema);
				sema.Wait ();
				StartWeb();
			} else if(Settings.ControlPort != ControlDel.port) {
				sema.Release ();
				ControlDel.Stop(sema);
				sema.Wait ();
				StartControl();
			}
		}
	}
}
