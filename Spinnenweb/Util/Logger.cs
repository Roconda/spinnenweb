using System;
using System.Threading;
using System.Collections.Concurrent;

namespace Spinnenweb
{
	public class Logger
	{
		private static int BUFFER_SIZE = 5;
		public static BlockingCollection<string> buffer = new BlockingCollection<string>(BUFFER_SIZE);

		public Logger ()
		{
		}

		public void Run() {
			Console.WriteLine("Started Logger");
		}
	}
}

