﻿using System;
using System.Collections.Generic;
using Mono.Data.Sqlite;
using System.Data;
using System.IO;
using System.Web.Security;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;

namespace Spinnenweb
{
	public class Database 
	{
		private SqliteConnection conn;
		public Database()
		{
			string db = "mydb.db3";
			bool exists = File.Exists (db);
			if (!exists)
				SqliteConnection.CreateFile (db);

			conn = new SqliteConnection("Data Source=" + db);

			string createTableQuery = @"CREATE TABLE IF NOT EXISTS [users] (
                          [id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                          [name] VARCHAR(2048)  NOT NULL,
                          [password] VARCHAR(2048)  NOT NULL,
                          [role] VARCHAR(2048)  NOT NULL
                          );
                          CREATE TABLE IF NOT EXISTS [sessions] (
                          [id] INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                          [user_id] INTEGER NOT NULL NOT NULL,
                          [key] VARCHAR(2048),
                          [time] DATETIME
                          );";

			using (SqliteCommand command = new SqliteCommand (conn)) {
				conn.Open ();
				command.CommandText = createTableQuery;   
				command.ExecuteNonQuery ();    
			}

			if (!exists) {
				InsertUser ("tim", "iscool", "beheerder");
				InsertUser ("nanne", "appelstroop", "ondersteuner");
			}
		}

		public User? IsLoggedIn(String session)
		{
			var query = "SELECT [users].* FROM [sessions], [users] WHERE key = @key and [users].id = [sessions].user_id and [sessions].time > @time";
			using (var command = new SqliteCommand(query, conn))
			{
				command.Parameters.Add (new SqliteParameter ("@key", DbType.String) { Value = session });
				command.Parameters.Add (new SqliteParameter ("@time", DbType.DateTime) { Value = DateTime.Now.Subtract(TimeSpan.FromHours(1)) });
				
				using (SqliteDataReader reader = command.ExecuteReader())
				{
					if (reader.Read ()) {
						var user = new User { 
							Id = reader.GetInt32 (reader.GetOrdinal ("id")),
							Username = reader ["name"] as string,
							Password = reader ["password"] as string,
							Role = reader ["role"]as string
						};
						return user;
					} else {
						return null;
					}
				}
			}
		}

		public User GetUser(int id)
		{
			var query = "SELECT * FROM [users] WHERE id = @id";
			using (var command = new SqliteCommand(query, conn))
			{
				command.Parameters.Add (new SqliteParameter ("@id", DbType.Int32) { Value = id });
				using (SqliteDataReader reader = command.ExecuteReader())
				{
					var users = new List<User> ();
					if (reader.Read ()) {
						return new User { 
							Id = reader.GetInt32(reader.GetOrdinal("id")),
							Username = reader ["name"] as string,
							Password = reader ["password"] as string,
							Role = reader ["role"]as string
						};
					} 
					return new User ();
				}
			}
		}

		public List<User> ListUsers()
		{
			var query = "SELECT * FROM [users]";
			using (var command = new SqliteCommand(query, conn))
			{
				using (SqliteDataReader reader = command.ExecuteReader())
				{
					var users = new List<User> ();
					while(reader.Read ()) {
						users.Add(new User { 
							Id = reader.GetInt32(reader.GetOrdinal("id")),
							Username = reader ["name"] as string,
							Password = reader ["password"] as string,
							Role = reader ["role"]as string
						});
					} 
					return users;
				}
			}
		}

		public bool InsertUser(string username, string password, string role)
		{
			var query = "INSERT INTO [users] VALUES(NULL, @username, @password, @role);";
			using (var command = new SqliteCommand (query, conn)) {
				command.Parameters.Add (new SqliteParameter ("@username", DbType.String) { Value = username.ToLower() });
				command.Parameters.Add (new SqliteParameter ("@password", DbType.String) { Value = encrypt(password, username.ToLower()) });
				command.Parameters.Add (new SqliteParameter ("@role", DbType.String) { Value = role });
				return command.ExecuteNonQuery () == 1;
			}
		}

		public bool ChangeUser(int id, string username, string password, string role)
		{
			var query = "UPDATE [users] SET name = @username, password = @password, role = @role where id = @id";
			using (var command = new SqliteCommand (query, conn)) {
				command.Parameters.Add (new SqliteParameter ("@username", DbType.String) { Value = username.ToLower() });
				command.Parameters.Add (new SqliteParameter ("@password", DbType.String) { Value = encrypt(password, username.ToLower()) });
				command.Parameters.Add (new SqliteParameter ("@role", DbType.String) { Value = role });
				command.Parameters.Add (new SqliteParameter ("@id", DbType.Int32) { Value = id });
				return command.ExecuteNonQuery () == 1;
			}
		}

		public bool DeleteUser(int id)
		{
			var query = "DELETE FROM [users] where id = @id";
			using (var command = new SqliteCommand (query, conn)) {
				command.Parameters.Add (new SqliteParameter ("@id", DbType.Int32) { Value = id });
				return command.ExecuteNonQuery () == 1;
			}
		}

		public User? Login (string username, string password)
		{
			var query = "SELECT * FROM [users] WHERE [name] = @username and [password] = @password";
			using (var command = new SqliteCommand(query, conn))
			{
				command.Parameters.Add (new SqliteParameter ("@username", DbType.String) { Value = username.ToLower() });
				command.Parameters.Add (new SqliteParameter ("@password", DbType.String) { Value = encrypt(password, username.ToLower()) });

				using (SqliteDataReader reader = command.ExecuteReader())
				{
					if(reader.Read ()) {
						var user = new User { 
							Id = reader.GetInt32(reader.GetOrdinal("id")),
							Username = reader ["name"] as string,
							Password = reader ["password"] as string,
							Role = reader ["role"]as string
						};

						var sessquery = "INSERT INTO [sessions] VALUES (NULL, @user_id, @key, datetime('now','localtime'));";
						using (SqliteCommand newsess = new SqliteCommand(sessquery, conn))
						{
							var key = Membership.GeneratePassword(255, 50);

							newsess.Parameters.Add (new SqliteParameter ("@user_id", DbType.Int32) { Value = user.Id });
							newsess.Parameters.Add (new SqliteParameter ("@key", DbType.String) { Value = key });

							newsess.ExecuteNonQuery();
							user.SessionKey = key;
						}

						return user;
					} else {
						return null;
					}
				}
			}
		}

		public void Logout (string sesskey)
		{
			var query = "DELETE FROM [sessions] WHERE key = @key;";
			using (var command = new SqliteCommand (query, conn)) {
				command.Parameters.Add (new SqliteParameter ("@key", DbType.String) { Value = sesskey });
				command.ExecuteNonQuery ();
			}
		}

		private String encrypt(String password, String hashToken) {
			// min length 6
			var salt = System.Text.Encoding.UTF8.GetBytes("heelveel"+ hashToken);

			string ret = null;
			using (var deriveBytes = new Rfc2898DeriveBytes(password, salt ))
			{
				byte[] hash = deriveBytes.GetBytes(20);

				ret = Encoding.UTF8.GetString(hash, 0, hash.Length);
			}
			return ret;
		}
	}

	public struct User {
		public Int32 Id;
		public string Username;
		public string Password;
		public string Role;

		public string SessionKey;
	}
//
//	public class User {
//		public int Id { get; set; }
//		public string Username { get; set; }
//		public string Password { get; set; }
//
//		public bool IsLoggedIn { get; set; }
//
//		public string SessionKey { get; set; }
//
//	}
}