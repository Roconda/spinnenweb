using System;
using System.Threading;
using System.Timers;
using System.IO;

namespace Spinnenweb
{
	public class LogWriter
	{
		public static string LOG_FILE = @"log.txt";

		public LogWriter ()
		{
		}

		public void Run() {
			Consume();
		}

		public void Consume() {
			var file = File.Open (LOG_FILE, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read);
			var writer = new StreamWriter (file);
			writer.BaseStream.Seek(0, SeekOrigin.End);


			while(true) {
				string logEntry = Logger.buffer.Take();

				writer.WriteLine (logEntry);
				writer.Flush();
				Console.WriteLine(logEntry);
			}
		}
	}
}

